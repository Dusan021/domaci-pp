<!DOCTYPE html>
<html>
  <head>
    <meta >
<link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <?php
      $nameErr = $lnameErr = $privacyErr =  "";
      $name = $lname = $privacy =  "";

      if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (empty($_POST["name"])) {
          $nameErr = "Name is required";
        }

        else {
          $name = test_input($_POST["name"]);

          if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
            $nameErr = "Only letters and white space allowed";
          }
        }
        if (strlen($name) < 2) {
          $nameErr = "min 2 letters";
        }

        //LAST NAME-------------------

        if (empty($_POST["lname"])) {
            $lnameErr = "Last name is required";
          }
          else {
            $lname = test_input($_POST["lname"]);

            if (!preg_match("/^[a-zA-Z ]*$/",$lname)) {
              $lnameErr = "Only letters and white space allowed";
            }
          }
          if (strlen($lname) < 2) {
            $lnameErr = "min 2 letters";
          }
          if (empty($_POST["privacy"])) {
            $privacyErr = "Directory is required";
          } else {
            $privacy = test_input($_POST["privacy"]);
          }






        //PARAMS----------------------


        if (!empty($nameErr) or !empty($lnameErr) or !empty($privacyErr) ) {
          $params = "name=" . urlencode($_POST["name"]);
          $params .= "&lname=" . urlencode($_POST["lname"]);
          $params .= "privacy=" . urlencode($_POST["privacy"]);

          $params = "&nameErr=" . urlencode($nameErr);
          $params .= "&lnameErr=" . urlencode($lnameErr);
          $params .= "&privacyErr=" . urlencode($privacyErr);





          header("Location: index.php?" . $params);
        }

        else {
          echo "<h2>Your Input:</h2>";
          echo "Name: " . $_POST['name'];
          echo "<br>";
          echo "Last name: " . $_POST['lname'];
          echo "<br>";


          echo "<a href=\"index.php\">Return to form</a>";
        }


      }

      function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
      }


     ?>

    <?php






      if(isset($_FILES["file"]) AND is_uploaded_file($_FILES['file']['tmp_name'])) {

        $file_name = $_FILES['file']["name"];
        $file_temp = $_FILES["file"]["tmp_name"];
        $file_size = $_FILES["file"]["size"];
        $file_type = $_FILES ['file']["type"];
        $file_error = $_FILES['file']["error"];



        if ($file_type > 0) {
          echo "Something went wrong during file upload!";
        }

        $target = "images/";
        $target = $target . basename( $_FILES['file']['name']) ;
        $ok=1;

        //This is our size condition


        $types = array('image/jpeg', );

        if (in_array($_FILES['file']['type'], $types)) {
          // file is okay continue
        } else {
          $ok=0;
          }

          //Here we check that $ok was not set to 0 by an error
          if ($ok==0){
            Echo "Sorry your file was not uploaded. It may be the wrong filetype. We only allow JPG filetypes.";
          }



        else {

          $ext_temp = explode(".", $file_name);
          $extension = end($ext_temp);
          $ime = $ext_temp[0];
          $random = rand(1,10);

          $selected_radio = $_POST['privacy'];
          $file_name_name = "$ime";
          $new_file_name = "$ime" . ".$extension";
          $upload = "$selected_radio/$new_file_name";



          if($selected_radio == "private") {
            if (!is_dir($selected_radio))
              mkdir($selected_radio);
          }
          if($selected_radio == "public") {
            if (!is_dir($selected_radio))
              mkdir($selected_radio);
          }




          if (!file_exists($upload)) {
            if (move_uploaded_file($file_temp, $upload)) {

              $size = getimagesize($upload);
              foreach ($size as $key => $value)
              echo "<br />$key = $value";

              echo "<img src=\"$upload\" $size[3] border=\"0\" alt=\"$file_name\" />";

            }
            else
              echo "<p><b>Error!</b></p>";
            }
            else {
              for($i=$random; $i<11; $i++) {
                if(!file_exists($selected_radio.'/'.$file_name_name.$i.'.'.$extension)){
                  move_uploaded_file($file_temp, $selected_radio.'/'.$file_name_name."-". date("YmdHis")."-" .$random.'.'.$extension);
                  break;
                }
              }
            }


        }


      }


    ?>
  </body>
</html>
